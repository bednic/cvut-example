import {api} from "@/plugins/axios";

const state = () => ({
    items: []
});

const getters = {};

const actions = {
    getAll: async ({commit}: any): Promise<void> => {
        const res = await api.get('https://f98a03a3-1b0d-4c7d-8d5c-89ccabd4c2bf.mock.pstmn.io/items');
        commit('setItems', res.data)
    }
};
const mutations = {
    setItems: (_state: any, items: []) => {
        _state.items = items;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
